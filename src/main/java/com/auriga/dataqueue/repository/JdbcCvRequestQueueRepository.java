package com.auriga.dataqueue.repository;

import com.auriga.dataqueue.CvRequest;
import com.auriga.dataqueue.tasks.ResultType;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Optional;


@Repository
public class JdbcCvRequestQueueRepository implements CvRequestQueueRepository {
    private final static String PUSH_NEW_QUEUE = "INSERT INTO public.item_queue( " +
            "            status, created_at, updated_at, item) " +
            "    VALUES ('IN_QUEUE', now(), now(), :item);";

    private final static String PULL_TOP_QUEUE = "update public.item_queue  " +
            "set status = 'IN_PROGRESS', " +
            "    updated_at = now(), " +
            "    attempts_count = attempts_count + 1 " +
            "where id in (select id FROM public.item_queue " +
            "              where (status = 'IN_QUEUE' or " +
            "              (DATE_PART('day', now() - updated_at) * 24 + DATE_PART('hour', now() - updated_at) > 1 " +
            "                   and status = 'IN_PROGRESS')) " +
            "              ORDER BY id limit 1 for update skip locked) " +
            "              returning *;";

    private final static String GET_BY_ID = "select * FROM public.item_queue " +
            "                          where id = :id; ";

    private final static String FINISH_REQUEST_BY_ID_PREFIX = "update public.item_queue  " +
            "set status = :status, " +
            "    updated_at = now() ";

    private final static String FINISH_REQUEST_BY_ID_SUFFIX = "where (id = :id and status = 'IN_PROGRESS');";

    private final static String GET_NUBMER_OF_WAITING_REQUESTS = "select count(*) " +
            "from item_queue " +
            "where ((status = 'IN_QUEUE') or (DATE_PART('day', now() - updated_at) * 24 + DATE_PART('hour', now() - updated_at) > 1 " +
            "                               and status = 'IN_PROGRESS'))";


    private NamedParameterJdbcTemplate template2;
    private RowMapper<CvRequest> cvRequestMapper = (rs, rowNum) -> {
        CvRequest cvRequest = new CvRequest();
        cvRequest.setId(rs.getInt("id"));
        cvRequest.setCvRequestStatus(CvRequestStatus.parse(rs.getString("status")));
        cvRequest.setCreatedAt(rs.getObject("created_at", LocalDateTime.class));
        cvRequest.setUpdatedAt(rs.getObject("updated_at", LocalDateTime.class));
        cvRequest.setItem(rs.getString("item"));
        cvRequest.setErrorMessage(rs.getString("error_message"));
        cvRequest.setAttemptsCount(rs.getInt("attempts_count"));
        cvRequest.setResult(rs.getString("result"));
        cvRequest.setResultType(ResultType.parse(rs.getString("result_type")));

        return cvRequest;
    };

    public JdbcCvRequestQueueRepository() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.ds.PGSimpleDataSource");
        dataSource.setUrl("jdbc:postgresql://localhost:5432/queuedb");
        dataSource.setUsername("postgres");
        dataSource.setPassword("");
        template2 = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public int push(String item) {
        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("item", item);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        template2.update(PUSH_NEW_QUEUE, namedParameters, keyHolder, new String[]{"id"});

        return keyHolder.getKey().intValue();
    }

    @Override
    public Optional<CvRequest> pull() {
        try {
            return Optional.ofNullable(template2.queryForObject(PULL_TOP_QUEUE, new MapSqlParameterSource(), cvRequestMapper));
        } catch (DataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public int updateCvRequest(int id, CvRequestStatus cvRequestStatus, String result, String errorMessage) {
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        StringBuilder stringBuilder = new StringBuilder(FINISH_REQUEST_BY_ID_PREFIX);
        namedParameters.addValue("id", id);
        namedParameters.addValue("status", cvRequestStatus.getValue());

        if (errorMessage != null) {
            stringBuilder.append(", error_message = :error_message ");
            namedParameters.addValue("error_message", errorMessage);
        }
        if (result != null) {
            stringBuilder.append(", result = :result ");
            namedParameters.addValue("result", result);
        }
        stringBuilder.append(FINISH_REQUEST_BY_ID_SUFFIX);

        return template2.update(stringBuilder.toString(), namedParameters);
    }

    @Override
    public Optional<CvRequest> getById(int id) {
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("id", id);
        try {
            return Optional.ofNullable(template2.queryForObject(GET_BY_ID, namedParameters, cvRequestMapper));
        } catch (DataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public int getNumberOfWaitingRequests() {
        return template2.queryForObject(GET_NUBMER_OF_WAITING_REQUESTS, new MapSqlParameterSource(), Integer.class);
    }
}
