package com.auriga.dataqueue.repository;

import java.util.ArrayList;
import java.util.List;

public enum CvRequestStatus {
    IN_QUEUE("IN_QUEUE"),
    IN_PROGRESS("IN_PROGRESS"),
    COMPLETED("COMPLETED"),
    FAILED("FAILED");

    private String value;

    CvRequestStatus(String status) {
        this.value = status;
    }

    public static List<CvRequestStatus> getCvRequestStatuses(String[] cvRequestStatuses) {
        List<CvRequestStatus> cvRequestStatusList = new ArrayList<>();
        if (cvRequestStatuses != null) {
            for (int i = 0; i < cvRequestStatuses.length; i++) {
                CvRequestStatus queueStatus = parse(cvRequestStatuses[i]);
                if (queueStatus != null)
                    cvRequestStatusList.add(queueStatus);
            }
        }
        return cvRequestStatusList;
    }

    public static List<String> getValues(List<CvRequestStatus> cvRequestStatuses) {
        List<String> cvRequestStatusList = new ArrayList<>();
        for (CvRequestStatus queueStatus : cvRequestStatuses)
            cvRequestStatusList.add(queueStatus.toString());
        return cvRequestStatusList;
    }

    public static CvRequestStatus parse(String value) {
        for (CvRequestStatus cvRequestStatus : values()) {
            if (cvRequestStatus.value.equalsIgnoreCase(value)) {
                return cvRequestStatus;
            }
        }
        return null;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}




