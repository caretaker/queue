package com.auriga.dataqueue.repository;


import com.auriga.dataqueue.CvRequest;

import java.util.Optional;

public interface CvRequestQueueRepository {


    int push(String item);

    Optional<CvRequest> pull();

    int updateCvRequest(int id, CvRequestStatus cvRequestStatus, String result, String errorMessage);

    Optional<CvRequest> getById(int id);

    int getNumberOfWaitingRequests();
}
