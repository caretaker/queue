package com.auriga.dataqueue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataqueueApplication {


    public static void main(String[] args) {
        SpringApplication.run(DataqueueApplication.class, args);
    }

}
