package com.auriga.dataqueue;

import com.auriga.dataqueue.repository.CvRequestStatus;
import com.auriga.dataqueue.tasks.ResultType;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

public class CvRequest implements Serializable {
    private int id;
    private CvRequestStatus cvRequestStatus;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private String item;
    private String errorMessage;
    private int attemptsCount;
    private String result;
    private ResultType resultType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CvRequestStatus getCvRequestStatus() {
        return cvRequestStatus;
    }

    public void setCvRequestStatus(CvRequestStatus сvRequestStatus) {
        this.cvRequestStatus = сvRequestStatus;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public int getAttemptsCount() {
        return attemptsCount;
    }

    public void setAttemptsCount(int attemptsCount) {
        this.attemptsCount = attemptsCount;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public ResultType getResultType() {
        return resultType;
    }

    public void setResultType(ResultType resultType) {
        this.resultType = resultType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CvRequest cvRequest = (CvRequest) o;
        return id == cvRequest.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "CvRequest{" +
                "id=" + id +
                ", cvRequestStatus=" + cvRequestStatus +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", item='" + item + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", attemptsCount=" + attemptsCount +
                '}';
    }
}
