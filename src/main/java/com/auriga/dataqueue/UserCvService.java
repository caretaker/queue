package com.auriga.dataqueue;


import com.auriga.dataqueue.service.CvRequestService;

/**
 * Сервис подачи заявок на создание CV
 * Возможны следующие действия:
 * 1) подача новой заявки
 * 2) получения статуса по уже выполненой
 */
public class UserCvService {
    CvRequestService cvRequestService;

    public UserCvService(CvRequestService cvRequestService) {
        this.cvRequestService = cvRequestService;
    }

    /**
     * Регистрирует заявку
     *
     * @param item - тело с данными (сами скиллы в готовом для создания cv формате)
     *             или же (юзер id и сами тянем уже здеть все данные и пихаем их в cvRequestService)
     * @return номер заявки
     */
    public int createNewCvRequest(String item) {

        return cvRequestService.registerNewRequest(item);
    }

    /**
     * Получает информацию о заявке
     *
     * @param id
     * @return заявку (нужно будет сделать обёртку другую, в которой не будет item'a тяжелого)
     */
    public CvRequest getInfoAboutCvRequest(int id) {

        return cvRequestService.getCvRequestById(id);
    }

}
