package com.auriga.dataqueue.tasks.Request;

import com.auriga.dataqueue.tasks.CvDto;
import com.auriga.dataqueue.tasks.Response.PdfResponseParameters;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


public class PdfRequestParameters extends AbstractRequestParameters<PdfResponseParameters> {
    private CvDto cvDto;

    @JsonCreator
    public PdfRequestParameters(@JsonProperty("CvDto") CvDto cvDto) {
        this.cvDto = cvDto;
    }
}
