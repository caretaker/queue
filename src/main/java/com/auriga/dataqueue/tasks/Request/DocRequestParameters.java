package com.auriga.dataqueue.tasks.Request;

import com.auriga.dataqueue.tasks.CvDto;
import com.auriga.dataqueue.tasks.Response.DocResponseParameters;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DocRequestParameters extends AbstractRequestParameters<DocResponseParameters> {
    private CvDto cvDto;

    @JsonCreator
    public DocRequestParameters(@JsonProperty("CvDto") CvDto cvDto) {
        this.cvDto = cvDto;
    }
}
