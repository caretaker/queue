package com.auriga.dataqueue.tasks.Request;

import com.auriga.dataqueue.tasks.RequestParameters;
import com.auriga.dataqueue.tasks.ResponseParameters;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class AbstractRequestParameters<T extends ResponseParameters> implements RequestParameters<T> {
    @JsonIgnore
    protected T response = null;

    public AbstractRequestParameters(T response) {
        this.response = response;
    }
    public AbstractRequestParameters() {
    }

    @Override
    public T getResponse() {
        return response;
    }

    @Override
    public void setResponse(T response) {
        this.response = response;
    }
}
