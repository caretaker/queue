package com.auriga.dataqueue.tasks.impl;

import com.auriga.dataqueue.tasks.AbstractTask;
import com.auriga.dataqueue.tasks.Request.DocRequestParameters;

public class DocTask extends AbstractTask<DocRequestParameters> {
    @Override
    protected void internalExecute(DocRequestParameters request) throws Exception {

        System.out.println("DOC task");
        request.getResponse().setCompleted(true);
    }
}
