package com.auriga.dataqueue.tasks;


import java.io.IOException;

public abstract class AbstractTask<T extends RequestParameters> implements Task<T> {


    //организация эксепшенов, найти решение

    @Override
    public void execute(T t) throws TaskException {
        try {
            internalExecute(t);
            t.getResponse().setCompleted(true);
        } catch (IOException e) {
            //log exception
            throw new TaskException(e.getMessage());
        } catch (Exception e) {
            //log
            throw new TaskException("Unexpected exception " + e);

        }
    }


    protected abstract void internalExecute(T request) throws Exception;


}
