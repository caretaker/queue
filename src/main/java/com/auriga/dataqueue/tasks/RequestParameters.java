package com.auriga.dataqueue.tasks;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import static com.fasterxml.jackson.annotation.JsonTypeInfo.As.PROPERTY;
import static com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME;

@JsonTypeInfo(use = NAME, include = PROPERTY)
public interface RequestParameters<T extends ResponseParameters> {

    T getResponse();

    void setResponse(T response);

}
