package com.auriga.dataqueue.tasks;

public interface Task<T extends RequestParameters> {
    void execute(T request) throws TaskException;
}
