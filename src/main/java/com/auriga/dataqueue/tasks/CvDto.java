package com.auriga.dataqueue.tasks;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


public class CvDto {
    private String name;

    @JsonCreator
    public CvDto(@JsonProperty("name") String name) {
        this.name = name;
    }
}
