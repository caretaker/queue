package com.auriga.dataqueue.tasks;

import com.auriga.dataqueue.CvRequest;
import com.auriga.dataqueue.service.CvRequestService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Service
public class TaskManager {
    private final static int MAX_THREADS = 5;
    BlockingQueue<Runnable> queue;
    ThreadPoolExecutor executorService;
    private CvRequestService cvRequestService;
    private TaskFactory taskFactory;
    private RequestParametersFactory requestParametersFactory;
    private List<ResponseParameters> responseParametersList;


    public TaskManager(CvRequestService cvRequestService) {
        this.cvRequestService = cvRequestService;
        //Можно и SynchronousQueue которая идейно подходит, но тогда придётся обрабатывать эксепшены часто -> низкая производительность
        queue = new ArrayBlockingQueue<>(MAX_THREADS);
        executorService = new ThreadPoolExecutor(MAX_THREADS, MAX_THREADS, 0L, TimeUnit.MILLISECONDS, queue);
        taskFactory = new TaskFactory();
        requestParametersFactory = new RequestParametersFactory();
        responseParametersList = new ArrayList<>();
    }

    @Scheduled(fixedDelay = 1000)
    public void addNewTasks() {
        checkCompletedRequests();
        for (int i = 0; i < MAX_THREADS - executorService.getActiveCount(); i++) {
            CvRequest cvRequest = cvRequestService.getCurrentCvRequest();
            RequestParameters requestParameters = requestParametersFactory.createRequestParameters(cvRequest.getItem(), cvRequest.getId());
            responseParametersList.add(requestParameters.getResponse());
            Task task = taskFactory.createTask(requestParameters);
            executorService.execute(() -> {
                try {
                    task.execute(requestParameters);
                } catch (TaskException e) {
                    //метод который обрабатывает эту ситуацию
                }
            });
        }


    }

    private void checkCompletedRequests() {
        responseParametersList.stream().filter(ResponseParameters::isCompleted).forEach(responseParameters -> {
            //Можно ли в весь респонс добавить такие поля и методы как failed/succeed, errorMessage, и вывод в toString;
            //cvRequestService.updateCvRequestById(responseParameters.getRequestId(),responseParameters);
            responseParametersList.remove(responseParameters);
        });
    }


}
