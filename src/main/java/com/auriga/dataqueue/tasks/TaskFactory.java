package com.auriga.dataqueue.tasks;


import com.auriga.dataqueue.tasks.Request.DocRequestParameters;
import com.auriga.dataqueue.tasks.Request.PdfRequestParameters;
import com.auriga.dataqueue.tasks.impl.DocTask;
import com.auriga.dataqueue.tasks.impl.PdfTask;

import java.util.HashMap;
import java.util.Map;

public class TaskFactory {

    private static final Map<Class<? extends RequestParameters>, ResultType> typeMap = new HashMap<>();

    static {
        typeMap.put(DocRequestParameters.class, ResultType.DOC);
        typeMap.put(PdfRequestParameters.class, ResultType.PDF);
    }


    Task createTask(RequestParameters requestParameters) {
        ResultType type = typeMap.get(requestParameters.getClass());

        switch (type) {
            case DOC:
                return new DocTask();

            case PDF:
                return new PdfTask();
        }
        return null;
    }
}
