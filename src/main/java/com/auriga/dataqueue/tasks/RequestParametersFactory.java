package com.auriga.dataqueue.tasks;

import com.auriga.dataqueue.tasks.Request.DocRequestParameters;
import com.auriga.dataqueue.tasks.Request.PdfRequestParameters;
import com.auriga.dataqueue.tasks.Response.DocResponseParameters;
import com.auriga.dataqueue.tasks.Response.PdfResponseParameters;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.NamedType;

import java.util.HashMap;
import java.util.Map;

public class RequestParametersFactory {
    private static final ObjectMapper MAPPER = new ObjectMapper();
    private static final Map<Class<? extends RequestParameters>, ResultType> typeMap = new HashMap<>();

    static {
        MAPPER.registerSubtypes(new NamedType(DocRequestParameters.class, "Doc"));
        MAPPER.registerSubtypes(new NamedType(PdfRequestParameters.class, "Pdf"));
        typeMap.put(DocRequestParameters.class, ResultType.DOC);
        typeMap.put(PdfRequestParameters.class, ResultType.PDF);
    }

    RequestParameters createRequestParameters(String json, int requestId) {
        try {
            RequestParameters requestParameters = MAPPER.readValue(json, RequestParameters.class);
            ResultType resultType = typeMap.get(requestParameters.getClass());
            switch (resultType) {
                case DOC:
                    requestParameters.setResponse(new DocResponseParameters(requestId));
                    return requestParameters;
                case PDF:
                    requestParameters.setResponse(new PdfResponseParameters(requestId));
                    return requestParameters;
            }

            //создать эксепшен и обернуть в него все следующие пробросы
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            // not implemented type of request
            // null только если нет такого resultType
        }
        return null;

    }

}
