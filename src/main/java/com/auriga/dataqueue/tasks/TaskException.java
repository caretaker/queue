package com.auriga.dataqueue.tasks;


public class TaskException extends Exception {
    TaskException(String message) {
        super(message);

    }
}
