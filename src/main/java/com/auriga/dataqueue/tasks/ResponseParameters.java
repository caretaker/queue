package com.auriga.dataqueue.tasks;

public interface ResponseParameters {
    boolean isCompleted();

    void setCompleted(boolean completed);

    int getRequestId();

    void setRequestId(int requestId);

}
