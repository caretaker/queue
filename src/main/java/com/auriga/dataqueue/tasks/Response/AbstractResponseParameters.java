package com.auriga.dataqueue.tasks.Response;

import com.auriga.dataqueue.tasks.ResponseParameters;

public abstract class AbstractResponseParameters implements ResponseParameters {
    private int requestId;
    private boolean completed;

    public AbstractResponseParameters(int requestId) {
        this.requestId = requestId;
        this.completed = false;
    }

    @Override
    public int getRequestId() {
        return requestId;
    }

    @Override
    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    @Override
    public boolean isCompleted() {
        return completed;
    }

    @Override
    public void setCompleted(boolean completed) {
        this.completed = completed;
    }


}
