package com.auriga.dataqueue.tasks.Response;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DocResponseParameters extends AbstractResponseParameters {

    @JsonCreator
    public DocResponseParameters(@JsonProperty("requestId") int requestId) {
        super(requestId);
    }
}
