package com.auriga.dataqueue.tasks.Response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PdfResponseParameters extends AbstractResponseParameters {

    @JsonCreator
    public PdfResponseParameters(@JsonProperty("requestId") int requestId) {
        super(requestId);
    }
}
