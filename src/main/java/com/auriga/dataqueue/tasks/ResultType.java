package com.auriga.dataqueue.tasks;

import java.util.ArrayList;
import java.util.List;

public enum ResultType {
    PDF("PDF"),
    DOC("DOC");

    private String value;

    ResultType(String type) {
        this.value = type;
    }

    public static List<ResultType> getResultTypes(String[] resultTypes) {
        List<ResultType> resultTypeList = new ArrayList<>();
        if (resultTypes != null) {
            for (int i = 0; i < resultTypes.length; i++) {
                ResultType queueStatus = parse(resultTypes[i]);
                if (queueStatus != null)
                    resultTypeList.add(queueStatus);
            }
        }
        return resultTypeList;
    }

    public static List<String> getValues(List<ResultType> resultTypes) {
        List<String> resultTypeList = new ArrayList<>();
        for (ResultType resultType : resultTypes)
            resultTypeList.add(resultType.toString());
        return resultTypeList;
    }

    public static ResultType parse(String value) {
        for (ResultType resultType : values()) {
            if (resultType.value.equalsIgnoreCase(value)) {
                return resultType;
            }
        }
        return null;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
