package com.auriga.dataqueue.service;

import com.auriga.dataqueue.CvRequest;
import com.auriga.dataqueue.repository.CvRequestQueueRepository;
import com.auriga.dataqueue.repository.CvRequestStatus;
import org.springframework.stereotype.Service;

@Service
public class CvRequestServiceImpl implements CvRequestService {
    CvRequestQueueRepository queueRepository;

    public CvRequestServiceImpl(CvRequestQueueRepository queueRepository) {
        this.queueRepository = queueRepository;
    }

    @Override
    public int registerNewRequest(String item) {
        return queueRepository.push(item);
    }

    @Override
    public CvRequest getCurrentCvRequest() {
        return queueRepository.pull().orElseThrow(() -> new RuntimeException("No more unfinished requests for now"));
    }

    @Override
    public CvRequest getCvRequestById(int id) {
        return queueRepository.getById(id).orElseThrow(() -> new RuntimeException("No request with such Id"));
    }

    @Override
    public void updateCvRequestById(int id, CvRequestStatus cvRequestStatus, String result, String errorMessage) {
        if (queueRepository.updateCvRequest(id, cvRequestStatus, result, errorMessage) == 0) {
            CvRequest cvRequest = queueRepository.getById(id).orElseThrow(() -> new RuntimeException("No request with such Id"));

            throw new RuntimeException("Request has status " + cvRequest.getCvRequestStatus() + " before update.");
        }
    }

    @Override
    public int getNumberOfWaitingRequests() {
        return queueRepository.getNumberOfWaitingRequests();
    }
}
