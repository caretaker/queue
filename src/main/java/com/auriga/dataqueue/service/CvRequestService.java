package com.auriga.dataqueue.service;

import com.auriga.dataqueue.CvRequest;
import com.auriga.dataqueue.repository.CvRequestStatus;

public interface CvRequestService {

    int registerNewRequest(String item);

    CvRequest getCurrentCvRequest();

    CvRequest getCvRequestById(int id);

    void updateCvRequestById(int id, CvRequestStatus cvRequestStatus, String result, String errorMessage);

    int getNumberOfWaitingRequests();
}
