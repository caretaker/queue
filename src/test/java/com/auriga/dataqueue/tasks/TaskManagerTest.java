package com.auriga.dataqueue.tasks;

import com.auriga.dataqueue.tasks.Request.DocRequestParameters;
import com.auriga.dataqueue.tasks.Request.PdfRequestParameters;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

class TaskManagerTest {
    private static final ObjectMapper MAPPER = new ObjectMapper();
    private final static int MAX_THREADS = 5;
    private static BlockingQueue<Runnable> queue;
    private static ThreadPoolExecutor executorService;
    private static TaskFactory taskFactory;
    private static RequestParametersFactory requestParametersFactory;
    private static ArrayList<ResponseParameters> responseParametersList;

    static {
        MAPPER.registerSubtypes(new NamedType(DocRequestParameters.class, "Doc"));
        MAPPER.registerSubtypes(new NamedType(PdfRequestParameters.class, "Pdf"));
        queue = new ArrayBlockingQueue<>(MAX_THREADS);
        executorService = new ThreadPoolExecutor(MAX_THREADS, MAX_THREADS, 0L, TimeUnit.MILLISECONDS, queue);
        taskFactory = new TaskFactory();
        requestParametersFactory = new RequestParametersFactory();
        responseParametersList = new ArrayList<>();
    }


    @Test
    void independentCompleteTest() throws JsonProcessingException {
        List<String> jsons = new ArrayList<>();
        //Параметры были записаны в бд
        CvDto cvDto = new CvDto("name");
        DocRequestParameters docRequestParameters = new DocRequestParameters(cvDto);
        PdfRequestParameters pdfRequestParameters = new PdfRequestParameters(cvDto);
        for (int i = 0; i < 200; i++) {
            jsons.add(MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(docRequestParameters));
            jsons.add(MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(pdfRequestParameters));
        }

        addTask(jsons);


    }


    void addTask(List<String> jsons) {
        for (int j = 0; j < 200; ) {
            if (executorService.getQueue().isEmpty()) {
                for (int i = 0; i < MAX_THREADS - executorService.getActiveCount(); i++) {
                    RequestParameters requestParameters = requestParametersFactory.createRequestParameters(jsons.get(j), i);
                    j = j + 1;
                    responseParametersList.add(requestParameters.getResponse());
                    Task task = taskFactory.createTask(requestParameters);
                    executorService.execute(() -> {
                        try {
                            task.execute(requestParameters);
                        } catch (TaskException e) {
                            //метод который обрабатывает эту ситуацию
                        }
                    });
                }
            }
            checkCompletedRequests();
        }


    }

    private void checkCompletedRequests() {
        List<ResponseParameters> completedResponseParametersList = responseParametersList
                .stream()
                .filter(ResponseParameters::isCompleted)
                .peek(responseParameters -> System.out.println(responseParameters.isCompleted()))
                .collect(Collectors.toList());
        System.out.println(completedResponseParametersList.size() + "completed");
        System.out.println(responseParametersList.size() + " before clear");
        responseParametersList.removeAll(completedResponseParametersList);
        System.out.println(responseParametersList.size() + " after clear");

    }
}