package com.auriga.dataqueue;

import com.auriga.dataqueue.repository.JdbcCvRequestQueueRepository;
import com.auriga.dataqueue.service.CvRequestService;
import com.auriga.dataqueue.service.CvRequestServiceImpl;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static com.fasterxml.jackson.annotation.JsonTypeInfo.As.PROPERTY;
import static com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME;

class CvRequestServiceImplTest {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    static {
        MAPPER.registerSubtypes(new NamedType(B.class, "B"));
        MAPPER.registerSubtypes(new NamedType(C.class, "C"));
    }

    @Test
    void registerNewRequest() throws InterruptedException {
        CvRequestService cvRequestService = new CvRequestServiceImpl(new JdbcCvRequestQueueRepository());
        ExecutorService executorService = Executors.newFixedThreadPool(5);


        int countOfRequestsBefore = cvRequestService.getNumberOfWaitingRequests();
        int countOfAddingRequests = 55;

        for (int i = 0; i < countOfAddingRequests; i++) {
            int finalI = i;
            executorService.execute(() -> {
                String item = "item num " + finalI;
                // System.out.println(cvRequestService.registerNewRequest(item));
            });
        }
        executorService.awaitTermination(2, TimeUnit.SECONDS);
        int countOfRequestsAfter = cvRequestService.getNumberOfWaitingRequests();

        assert (countOfRequestsAfter - countOfRequestsBefore == countOfAddingRequests);
    }

    @Test
    void parallelRequestProcessing() throws InterruptedException {
        CvRequestService cvRequestService = new CvRequestServiceImpl(new JdbcCvRequestQueueRepository());
        ExecutorService executorService = Executors.newFixedThreadPool(5);

        int countOfRequestsBefore = cvRequestService.getNumberOfWaitingRequests();
        AtomicInteger t = new AtomicInteger();
        for (int i = 0; i < 100; i++) {
            int finalI = i;
            executorService.execute(() -> {
                try {
                    CvRequest cvRequest = cvRequestService.getCurrentCvRequest();
                    t.set(t.get() + 1);
                    if (cvRequest.getAttemptsCount() > 3) {
                        //cvRequestService.updateCvRequestById(cvRequest.getId(), CvRequestStatus.FAILED, "TOO MANY COUNTS ");
                    } else {
                        if (processRequest()) {
                            //  cvRequestService.updateCvRequestById(cvRequest.getId(), CvRequestStatus.COMPLETED, "");
                        } else {
                            //cvRequestService.updateCvRequestById(cvRequest.getId(), CvRequestStatus.FAILED, "SOME ERROR");
                        }
                    }
                } catch (RuntimeException r) {
                    System.out.println("return null element" + finalI);
                    if (cvRequestService.getNumberOfWaitingRequests() == 0) {
                        executorService.shutdownNow();
                    }

                }
            });
        }

        executorService.awaitTermination(10, TimeUnit.SECONDS);

        int countOfRequestsAfter = cvRequestService.getNumberOfWaitingRequests();
        System.out.println(t.get());

        assert (countOfRequestsBefore - countOfRequestsAfter == t.get());
    }

    @Test
    public void lll() throws JsonProcessingException {
        B b = new B(213);
        A ab = new B(321);
        C c = new C(123.123);
        A ac = new C(213.213);
        final String jsonb = MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(b);
        final String jsonab = MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(ab);
        final String jsonc = MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(c);
        final String jsonac = MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(ac);

        A a = MAPPER.readValue(jsonb, A.class);
        A aa = MAPPER.readValue(jsonc, A.class);
        A aaa = MAPPER.readValue(jsonab, A.class);
        A aaaa = MAPPER.readValue(jsonac, A.class);
        HashMap<Class, String> hashMap = new HashMap<>();
        hashMap.put(A.class, "A");
        hashMap.put(B.class, "B");
        hashMap.put(C.class, "C");

        System.out.println(hashMap.get(a.getClass()));
        System.out.println(a instanceof B);
        System.out.println(a);
        System.out.println(aa);
        System.out.println(aaa);
        System.out.println(aaaa);

        Gggg g = new Gggg((B) a);

    }

    public String create(B b) {
        return "b";
    }

    public String create(C a) {
        return "a";
    }

    private boolean processRequest() {

        return Math.random() * 100 > 5f;
    }


    @JsonTypeInfo(use = NAME, include = PROPERTY)
    interface A {

        String getValue();

        public void setValue(String value);
    }


    static class B implements A {
        int num;

        @JsonCreator
        public B(@JsonProperty("num") int d) {
            this.num = d;
        }

        @Override
        public String getValue() {
            return String.valueOf(num);
        }

        @Override
        public void setValue(String value) {
            num = Integer.parseInt(value);

        }

        @Override
        public String toString() {
            return "B";
        }
    }

    static class C implements A {
        double pup;

        @JsonCreator
        public C(@JsonProperty("pup") double d) {
            this.pup = d;
        }

        @Override
        public String getValue() {
            return String.valueOf(pup);
        }

        @Override
        public void setValue(String value) {
            pup = Double.parseDouble(value);

        }

        @Override
        public String toString() {
            return "C";
        }
    }

    class Gggg {
        Gggg(B b) {
            b.num++;
        }
    }

}